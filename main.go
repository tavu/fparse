package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

// FluentData type
// Example log data:{"container_id":"xxx","container_name":"/foo.127.0.0.1.xip.io","source":"stdout","log":"\t\t\t\tsites {"}
type FluentData struct {
	ContainerID   string `json:"container_id"`
	ContainerName string `json:"container_name"`
	Source        string `json:"source"`
	Log           string `json:"log"`
}

var verbose bool

var rootCmd = &cobra.Command{
	Use:     "fparse [OPTION]... [FILE]...",
	Short:   "fparse prettify fluent log output",
	Long:    `fparse prettify fluent log output"`,
	Example: `$ docker exec <name> tail -f /fluentd/log/data.log | fparse`,
	Args:    cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			line := scanner.Text()
			parts := strings.Split(line, "\t")
			jsonBlob := []byte(parts[2])
			var data FluentData
			err := json.Unmarshal(jsonBlob, &data)
			if err != nil {
				fmt.Println("fparse error:", err)
			} else {
				fmt.Printf("%s\t%s\t%s\n", parts[0], parts[1], data.Log)
			}
		}
		if scanner.Err() != nil {
			os.Exit(1)
		}
	},
}

func init() {
	usageTempl := `{{ $cmd := . }}Usage:
  {{.UseLine}}

Flags:
{{.LocalFlags.FlagUsages}}
Examples:
  {{ .Example }}
`
	rootCmd.SetUsageTemplate(usageTempl)
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose output")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
